# Copyright 2018 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit appid cros-unibuild

DESCRIPTION="Ebuild which pulls in any necessary ebuilds as dependencies
or portage actions."

LICENSE="BSD-Google"
SLOT="0"
KEYWORDS="-* amd64 x86"
IUSE="hatch-borealis hatch-diskswap hatch-kvm hatch-kernelnext hatch-blueznext"
S="${WORKDIR}"

# Add dependencies on other ebuilds from within this board overlay
RDEPEND="
	chromeos-base/sof-binary
	chromeos-base/sof-topology
	media-sound/sound_card_init
"
DEPEND="
	${RDEPEND}
	chromeos-base/chromeos-config
"

src_install() {
	if use hatch-borealis; then
		doappid "{8CD2059E-B678-11EA-BEA0-BFD4C54FEC76}" "CHROMEBOOK"
	elif use hatch-diskswap; then
		doappid "{6FBDA804-5618-89BA-5D6E-F3804BDE0EF3}" "CHROMEBOOK"
	elif use hatch-kvm; then
		doappid "{4D5CCCEE-A214-4CFD-9A9F-85DFCF7A0CD4}" "CHROMEBOOK"
	elif use hatch-kernelnext; then
		doappid "{9DFA3334-7067-11EA-A862-83D15EF402B8}" "CHROMEBOOK"
	elif use hatch-blueznext; then
		doappid "{ED913761-52D5-4ED2-8043-598C58F2AAA0}" "CHROMEBOOK"
	else
		doappid "{95EE134E-B47F-43FB-9835-32C276865F9A}" "CHROMEBOOK"
	fi

	unibuild_install_audio_files

	unibuild_install_autobrightness_files
}
