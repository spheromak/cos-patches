# Copyright 2018 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# Initial value just for style purposes.
USE=""
LINUX_FIRMWARE=""

USE="${USE} nautilus"
USE="${USE} cros_ec"

# Disable ACPI AC device
USE="${USE} -acpi_ac"

# Use ext4 crypto
USE="${USE} direncryption"

# Google USBPD peripheral firmwares
LINUX_FIRMWARE="${LINUX_FIRMWARE} cros-pd"

# WiFi driver, BT driver, and USBPD peripheral firmwares
LINUX_FIRMWARE="${LINUX_FIRMWARE} iwlwifi-7265D ibt-hw"

# Add Intel Camera IMGU ipu3-fw.bin
LINUX_FIRMWARE="${LINUX_FIRMWARE} ipu3_fw"

# Build EC firmware from source
EC_FIRMWARE="nautilus"

# Add EC logging
USE="${USE} eclog"

USE="${USE} -tpm tpm2 cr50_onboard"

# Add keyboard
USE="${USE} keyboard_includes_side_buttons"

# Disable touchpad wakeup since it can't be turned off dynamically when the
# system is converted to tablet mode while it's suspended.
USE="${USE} -touchpad_wakeup"

# Add Touchview to get chromeos-accelerometer-init.
USE="${USE} touchview"

# Enable VMs.
USE="${USE} kvm_host crosvm-gpu virtio_gpu"

# Add modemfwd for updating modem firmware.
USE="${USE} modemfwd"

# To get wacom_flash
INPUT_DEVICES="wacom"

# Comment these lines to disable the serial port.
#TTY_CONSOLE="ttyS0"
#USE="${USE} pcserial"

# Enable background blur.
USE="${USE} background_blur"

# Enable CPU zero-copy for Chrome camera stack.
USE="${USE} video_capture_use_gpu_memory_buffer"

# Add IME addons for some optional IME features.
USE="${USE} ime_addons"

# Enable on-device handwriting recognition
USE="${USE} ondevice_handwriting"
